﻿using System;
using Component1;
using Component2;
using MainB;
using Component2B;
using ComponentFramework;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Class1);
        public static Type Component2 = typeof(Class2);

        public static Type RequiredInterface = typeof(Interface1);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => component2;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { };

        public static AreDependenciesResolved ResolvedDependencied = (container) => true;

        #endregion

        #region P3

        public static Type Component2B = typeof(Class3);

        #endregion
    }
}
