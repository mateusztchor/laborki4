﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Component1;
using Component2;
using MainB;

namespace Lab4.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();
            Class2 klasa2 = new Class2();
            klasa2.RegisterProvidedInterface<Interface1>(klasa2);
            kontener.RegisterComponent(klasa2);
            Class1 klasa1 = new Class1(kontener.GetInterface<Interface1>());
            klasa1.Metoda5();
            Console.ReadKey();
            Class4 klasa4 = new Class4();
            klasa4.Metoda1();
        }
    }
}
