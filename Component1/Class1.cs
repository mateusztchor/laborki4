﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Component2;

namespace Component1
{
    public class Class1 : AbstractComponent
    {
        Interface1 interface1;

        public Class1(Interface1 interface1)
        {
            this.interface1 = interface1;
        }

        public void Metoda5()
        {
            interface1.Metoda1();
        }

        public void Metoda6() { }

        public override void InjectInterface(Type type, object impl)
        {

        }
    }
}
