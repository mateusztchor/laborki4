﻿using Component1;
using Component2;
using Component2B;
using ComponentFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainB
{
    public class Class4
    {
        public void Metoda1()
        {
            Container kontener = new Container();
            Class3 klasa3 = new Class3();
            klasa3.RegisterProvidedInterface<Interface1>(klasa3);
            kontener.RegisterComponent(klasa3);
            Class1 klasa1 = new Class1(kontener.GetInterface<Interface1>());
            klasa1.Metoda5();
            Console.ReadKey();
        }
    }
}
