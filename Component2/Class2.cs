﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Component2
{
    public class Class2 : AbstractComponent, Interface1
    {
        public void Metoda1()
        {
            Console.WriteLine("Metoda1: Klasa2 (Component2)");
        }

        public void Metoda2()
        {
            Console.WriteLine("Metoda2: Klasa2 (Component2)");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
